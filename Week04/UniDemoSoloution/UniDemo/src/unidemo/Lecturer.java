/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author sthill
 */
public class Lecturer extends UniPeople implements ITeach{

    public Lecturer(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    
    @Override
    public void teach() {
        System.out.println(this.name + " is teaching " + this.course.getCode() + " in room " + this.course.getRoom());
    }

    @Override
    public void setCourseWork(String courseWork) {
        this.course.setCourseWork(courseWork);
    
    }   
    
  
}
