/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author sthill
 */
public class Course {
    private Lecturer teacher;
    private String courseWork;
    private String code;
    private String room;

    public Course(String room, String code) {
        this.room = room;
        this.code = code;
    }
    
    public Lecturer getTeacher() {
        return teacher;
    }

    public void setTeacher(Lecturer teacher) {
        this.teacher = teacher;
    }

    public String getCourseWork() {
        return courseWork;
    }

    public void setCourseWork(String courseWork) {
        this.courseWork = courseWork;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
}
