/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author sthill
 */
public class Admin {
    
    //Overloading because the the logic is different for lecturers and teachers
    public static void assignCourse(Lecturer teacher, Course course) {
        teacher.setCourse(course);
        course.setTeacher(teacher);
    }
    
    public static void assignCourse(Student student, Course course) {
        student.setCourse(course);
    }  
    
    //Polymorphism because the logic is the same
    public static void getDetails(UniPeople p) {
    
        System.out.println("Name: " + p.getName());
        System.out.println("ID: " + p.getId());
        
        if(p.getCourse() != null) {
            System.out.println("Course: " + p.getCourse().getCode());
        } else {
            System.out.println("No Course assigned");
        }
    }
}
