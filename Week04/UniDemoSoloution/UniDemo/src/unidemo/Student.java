/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author sthill
 */
public class Student extends UniPeople{

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public void attendClass() {
        System.out.println(this.name + " is attending " + this.course.getCode() + " in room " + this.course.getRoom());
    }
    
    public void doCourseWork() {
        
        if(this.course.getCourseWork() != null && !this.course.getCourseWork().isEmpty()) { 
            System.out.println(this.name + " is doing coursework " + this.course.getCourseWork());
        } else {
            System.out.println("No coursework set!");
        }
    }
    
}
