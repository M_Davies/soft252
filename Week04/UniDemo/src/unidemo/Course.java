/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author mdavies16
 */
public class Course {
    private String room = "UNKNOWN";
    private String code = "UNKNOWN";
    private Object teacher = "UNKNOWN";
    private String coursework = "UNKNOWN";

    public Course(String theCode, Object theTeacher, String theCoursework,
            String theRoom) {
        code = theCode;
        teacher = theTeacher;
        coursework = theCoursework;
        room = theRoom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        if (code != null && !code.isEmpty()) {
            this.code = code;
        }        
    }

    public Object getTeacher() {
        return teacher;
    }

    public void setTeacher(Object teacher) {
        this.teacher = teacher;
    }

    public String getCoursework() {
        return coursework;
    }

    public void setCoursework(String coursework) {
        this.coursework = coursework;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        if (room != null && !room.isEmpty()) {
            this.room = room;
        }
    }
}
