/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unidemo;

/**
 *
 * @author mdavies16
 */
public abstract class UniPeople {
    protected String course = "UNKNOWN";
    protected Integer id = 0;
    protected String name = "UNKNOWN";

    public UniPeople(Integer theId, String theName) {
        id = theId;
        name = theName;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    
}
