/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker.stockdatamodel;
import utilities.*;
import java.util.ArrayList;
/**
 *
 * @author toxic
 */
public abstract class StockItem implements ISubject{
    //Private attirbute values.
    protected String name = "UNKNOWN";
    protected Integer quantityInStock = 0;
    protected Double sellingPrice = 1.00;
    protected Double costPrice = 1.00;
    private ArrayList <IObserver> observers = null;
    
    public Boolean isInStock(){
        Boolean inStock = false;
        if (this.quantityInStock > 0) {
            inStock = true;
        }
        return inStock;
    }

    //Accessors (including business rules).
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null && !name.isEmpty()) {
            this.name = name;
            notifyObservers();
        }
        else{
            System.out.println("INVALID INPUT");
        }        
    }

    public Integer getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(Integer quantityInStock) {
        if (quantityInStock != null && quantityInStock >= 0) {
            this.quantityInStock = quantityInStock;
            notifyObservers();
        }
        else{
            System.out.println("INVALID INPUT");
        }            
    }

    public Double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(Double sellingPrice) {
        if (sellingPrice != null && sellingPrice >= this.costPrice 
                && sellingPrice >= 0){
           this.sellingPrice = sellingPrice;
           notifyObservers();
        }                   
        else{
            System.out.println("INVALID INPUT");
        }
    }
    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        if (costPrice != null && costPrice >= 0) {
            this.costPrice = costPrice;
            notifyObservers();
        }
        else{
            System.out.println("INVALID INPUT");
        }
    }
    
    public StockItem(){
    }
    
    public StockItem (String name)
    {
        this.name = name;
    }
    
    public StockItem(String name, Integer qty)
    {
        this.name = name;
        this.quantityInStock = qty;
    }
    
    public abstract StockType getItemType();
    
    //Observer methods
    @Override
    public Boolean registerObserver(IObserver o)
    {
        Boolean blnAdded = false;
        //Validate observer existance
        if(o != null) {
            //If observer list not init, create
            if (this.observers == null) {
                this.observers = new ArrayList<>();                
            }
            //Add observer to list
            blnAdded = this.observers.add(o);
            notifyObservers();
        }
        //Return updated list 
        return blnAdded;
    }
    
    @Override
    public Boolean removeObserver(IObserver o)
    {
        Boolean blnRemoved = false;
        //Validate observer existance
        if (o != null) {
            //Remove observer from list
            blnRemoved = this.observers.remove(o);  
            notifyObservers();
        }
        //Return updated list
        return blnRemoved;
    }
    
    @Override
    public void notifyObservers()
    {
        //Ensure valid list exists
        if (this.observers != null && this.observers.size() > 0) {
            //Foreach
            this.observers.forEach((currentObserver) -> {
                //Call update
                currentObserver.update();
            });
        }
    }
    
}
