/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stocktracker;

//Added methods to fix errors
import stocktracker.stockdatamodel.*;
/**
 *
 * @author toxic
 */
public class StockTracker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Test items: declared as stock items because its an IS-A
        //inheritance relationship
        StockItem objTestItem1 = new PhysicalStockItem("Starcraft Manual", 100);
        StockItem objTestItem2 = new ServiceStockItem("Delivery Fee");
        
        //Ask item to print type into console to test functionality
        //Item1
        if (objTestItem1.getItemType() == StockType.PHYSICALITEM) {
            System.out.println("Item 1 is a PHYSICAL stock item");
        }else{
            System.out.println("Item 1 is a SERVICE stock item");            
        }                  
        //Item2
        if (objTestItem2.getItemType() == StockType.PHYSICALITEM) {
            System.out.println("Item 2 is a PHYSICAL stock item");
        }else{
            System.out.println("Item 2 is a SERVICE stock item");            
        }
        
        //****************************************//
        //Create Phyiscal item, Service Stock Item and Observer.
//        PhysicalStockItem objPhysicalItem = 
//                new PhysicalStockItem("Snuff: A Discworld book by Terry Pratchett", 100);
//        ServiceStockItem objVirtualItem = 
//                new ServiceStockItem("Wintersmith: A Discworld ebook by Terry Pratchett");
        
        AnObserver objObserver = new AnObserver();
        
        //Register observer with both types of item.
        objTestItem1.registerObserver(objObserver);
        objTestItem2.registerObserver(objObserver);
        
        //Change qty of physical item.
        System.out.println("Changing quantity of physical stock item");
        objTestItem1.setQuantityInStock(87);
        
        //Change price of service item.
        System.out.println("Changing price of service stock item");
        objTestItem2.setSellingPrice(3.00);
        
        //Test behaviour of Physical Stock Item.
//        String strMessage1 = objPhysicalItem.getName()
//                + ", Is in stock = " + objPhysicalItem.isInStock()
//                + ", Qty is in stock: " + objPhysicalItem.getQuantityInStock();
//        System.out.println(strMessage1);
        
        String strMessage2 = objTestItem1.getName()
                + ", Is in stock = " + objTestItem1.isInStock()
                + ", Qty is in stock: " + objTestItem1.getQuantityInStock();
        System.out.println(strMessage2);
        
        //Test behaviour of Service Stock Item.
//        String strMessage3 = objVirtualItem.getName()
//                + ", Is in stock - " + objVirtualItem.isInStock()
//                + ", Qty is in stock: " + objVirtualItem.getQuantityInStock();
//        System.out.println(strMessage3);
        
        String strMessage4 = objTestItem2.getName()
                + ", Is in stock = " + objTestItem2.isInStock()
                + ", Qty is in stock: " + objTestItem2.getQuantityInStock() + 
                ". Price = " + objTestItem2.getSellingPrice();
        System.out.println(strMessage4);
    }
    
}
