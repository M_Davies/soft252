package ObserverModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Date;

/**
 * Interface that is implemented when a specific details has changed amongst the
 * observables. Whether that is the stock being updated, a new user added or an
 * appointment is created, the interface is implemented by the relevant classes
 * and will react to the notification
 * 
 * @author M_Davies
 */
public interface Observer {

    /**
     * Abstract method implemented by the administrator or secretary when creating
     * a new account. The subclass of a chosen type implements this method
     * and fills out the details from the user input
     * @param id Unique user id (concatenated letter with 4 randomly generated 
     * numbers) with the letter coming from whatever class is implementing the
     * method
     * @param forename New forename of the user
     * @param surname New surname of the user
     * @param address New address of the user (only patients use their home
     * address, all other accounts use their surgery address)
     * @param gender New gender for the patient (gender is a null value for
     * non-patients)
     * @param age New age for the patient (age is a null value for non-patients)
     * @param type Type of the account to create (Administrator, Patient, Doctor
     * or Secretary)
     */
    public void updateDetails(String id, String forename, String surname, String address,
            String gender, Integer age, String type);
    
    /**
     * Method to update the database of medicinal stock. The secretary and doctor
     * implement this (along with the stock class) when adding or removing stock
     * and creating new medicines respectively. 
     * @param medicine Medicine name
     * @param qty Quantity of medicine in stock (can be 0)
     */
    public void updateStock(String medicine, Integer qty);
    
    /**
     * Method to update the Appointment details when a secretary creates an
     * appointment or when a doctor makes a follow up check up. Both instances
     * use this method as a skeleton to fill out the finer details.
     * @param id Unique id of the doctor hosting the appointment
     * @param dateOfAppointment Date the new appointment will take place
     */
    public void updateAppointment(String id, Date dateOfAppointment);
}
