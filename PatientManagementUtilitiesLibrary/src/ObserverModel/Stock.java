package ObserverModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;

/**
 * This class implements the both the observer and observable interfaces and also
 * inherits the prescription class using weak aggregation.This was done so the 
 * only communication with the class would have to be through the observer
 * pattern and would alert each user involved to a change with the stock.
 * @author M_Davies
 */
public abstract class Stock extends Prescription implements Observer, Observable {
    private String medicine = "";
    private Integer qty = 0;
    private ArrayList stock = new ArrayList();
    private ArrayList observers = new ArrayList();

    /**
     * Constructor to initialise the class
     */
    public Stock() {
    }

    /**
     * Overrides the medicine value from the prescription to the new name 
     * currently selected in the stock. Used when retrieving the information
     * about currently stocked medicines. No need for a setter method since this 
     * is handled in the doctor class
     * @return medicine Name of the medicine
     */
    @Override
    public String getMedicine() {
        return medicine;
    }
    
    /**
     * Overrides the medicine quantity in stock from the prescription to the new 
     * value currently selected in the stock. Used when retrieving the quantity 
     * of currently stocked medicines. A setter method is needed here since the 
     * secretary will need to alter the quantity of the stock, usually when a 
     * patient collects a prescription
     * @return qty Quantity of the medicine in stock
     */   
    @Override
    public Integer getQty() {
        return qty;
    }

    /**
     * Validation checking for null and negative values is also included here 
     * (it shouldn't be possible to order a medicine without specifying the
     * quantity)
     * @param qty New quantity of the medicine in stock
     */
    @Override
    public void setQty(Integer qty) {
        if(qty != null && qty > 0)
            this.qty = qty;
    }

    /**
     * Accessor to retrieve the arraylist of all the stock items (included here 
     * are the names and quantity amounts of each medicine). Will be implemented
     * by the Secretary when reviewing currently stocked medicines.
     * @return stock List of all the currently stocked medicines and their amount
     * in stock
     */
    public ArrayList getStock() {
        return stock;
    }

    /**
     * Accessor to set the items in the arraylist of stock items. This is used when
     * the secretary orders new stock in, updating the stock to include the new
     * medicine ordered. Includes validation checking to ensure the stock always
     * has medicines in it (a clinic is no use without medicines)
     * @param stock Updated list of the currently stocked medicines plus any
     * newly added medicines
     */
    public void setStock(ArrayList stock) {
        if(stock != null && !stock.isEmpty())
            this.stock = stock;
    }

    /**
     * Accessor to get the list of the secretaries, patients and doctors so the
     * relevant parties can be subscribed to any news messages regarding an 
     * stock change (e.g. a patient returns to the clinic when they receive a 
     * message notifying them that the drug is back in stock).
     * @return observers ArrayList of users on the system 
     */
    public ArrayList getObservers() {
        return observers;
    }

     /**
     * Accessor to set the list of the secretaries, patients and doctors so new 
     * relevant parties can be subscribed to any news messages regarding an 
     * appointment (e.g. a new secretary should be added so they can be informed
     * of any stock changes).
     * @param observers Updated list of users on the system
     */
    public void setObservers(ArrayList observers) {
        this.observers = observers;
    }
    
    /**
     * Implementing the abstract method from the observer interface, the method
     * calls the set stock, quantity and medicine methods above. This is so that
     * when a new medicine is added, a current medicine's quantity in stock has 
     * changed or when a medicine is out or in stock, the relevant parties are 
     * informed of the change in the stock
     * @param medicine Medicine status to be altered
     * @param qty  Quantity of the medicine to be altered
     */
    @Override
    public void updateStock(String medicine, Integer qty){
        
    }
    
    /**
     * This method is implemented by the doctor through the observable to fill 
     * out their prescription on the patient. The prescription is stored here 
     * until the patient comes to collect it (seen in the method below). They're
     * informed of the change via the observer interface
     * @param medicine Medicine name to be collected
     * @param qty Medicine quantity to be removed from the stock (also checks
     * if the secretary actually has the medicine in stock)
     * @param dosage Dosage the patient should take of the medicine
     */
    public void prescribeMedicine(String medicine, Integer qty, String dosage){
        
    }
    
    /**
     * This method follows on from the one above. After a patient is notified that,
     * their prescription is ready, this method is called from prescribeMedicine().
     * It removes the quantity of the specified medicine from the secretaries 
     * stock and passes the medicine name and dosage values to the patient. 
     * @param medicine Medicine name to collect
     * @param qty Quantity of the medicine to be removed from the stock
     * @param dosage Dosage the patient should take of the medicine.
     */
    public void collectMedicine(String medicine, Integer qty, String dosage){
        
    }
}
