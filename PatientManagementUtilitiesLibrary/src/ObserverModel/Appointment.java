package ObserverModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.Date;

/**
 * This is a superclass of the Prescription class and implements the observable
 * and observer interface since it can be both depending on what action is 
 * being performed. This is where the concrete details of an appointment are 
 * created.
 * @author M_Davies
 */
public abstract class Appointment extends Prescription implements Observable, Observer {
    private String doctorName = getDoctorName();
    private Date dateOfAppointment = new Date();
    private ArrayList <Observer> observers;

    /**
     * Constructor to initialise the class
     */
    public Appointment() {
        observers = new ArrayList();
    }

    /**
     * Two accessor methods that implement the doctorName accessors in prescription.
     * These are used to add the doctors name to the prescription which would not 
     * be possible from the patient class (they're not subclasses of each other)
     * @return doctorName The concatenated doctor name from the Prescription
     * class
     */
    @Override
    public String getDoctorName() {
        return doctorName;
    }

    @Override
    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
        notifyObserver();
    }

    /**
     * Accessor to retrieve the date of the appointment. Used by the secretary
     * and doctor to view the dates of potential upcoming appointments
     * @return dateOfAppointment The date the appointment will take place
     */
    public Date getDateOfAppointment() {
        return dateOfAppointment;
    }

    /**
     * Accessor to set the appointment date. This is implemented by the doctor, 
     * patient and secretary when negotiating on an appointment date.
     * @param dateOfAppointment New date of the appointment (subject to change often)
     */
    public void setDateOfAppointment(Date dateOfAppointment) {
        this.dateOfAppointment = dateOfAppointment;
        notifyObserver();
    }
    
    /**
     * Used to add new relevant users to the subscription list
     * @param newObserver New observer to add
     */
    @Override
    public void registerObserver(Observer newObserver){
        observers.add(newObserver);
    }
    
    /**
     * Used to remove observers from the subscription list (e.g. if their account
     * has been deleted or their assistance is no longer required with the 
     * appointment setup)
     * @param deleteObserver Observer to be deleted
     */
    @Override
    public void removeObserver (Observer deleteObserver){
        Integer observerIndex = observers.indexOf(deleteObserver);
        observers.remove(observerIndex);
    }
    
    /**
     * Obtains the arraylist of all the users registered on the appointment.
     * This could theoretically notify uninvolved users about an appointment 
     * request, I would need more time to test this however.
     */
    @Override
    public void notifyObserver(){
        observers.forEach((observer) -> {
            observer.updateAppointment(doctorName, dateOfAppointment);
        });
    }
}
