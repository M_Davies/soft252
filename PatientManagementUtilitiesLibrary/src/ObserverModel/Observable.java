package ObserverModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Interface that is implemented when a welcome message needs to be broadcast
 * to all users involved (for example, when a patient requests to delete their
 * account, the secretary is notified)
 * @author M_Davies
 */
public interface Observable {
    /**
     * Register new observer. Used when creating new users so they are subscribed
     * to any system messages they may need to act on
     * @param observer Observer to add
     */
    public void registerObserver(Observer observer);

    /**
     * Remove observer. Used when deleting users from the system or when an
     * appointment is completed (no need to observe an appointment if it doesn't
     * exist). 
     * @param observer Observer to be removed
     */
    public void removeObserver(Observer observer);

    /**
     * Alerts the observer of a change of state in the program. This is called by
     * an involved observer for almost every change in the program
     * 
     */
    public void notifyObserver();

    /**
     * Specific method that needs to be performed by observable at the request 
     * of an observer. The request is an abstract method used when creating a
     * new Secretary, Doctor or Administrator account from the Administrator 
     * application.
     * @param type The type of user to create
     */
}
