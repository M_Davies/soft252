/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FactoryModel;

/**
 * Superclass for all the user classes. The class contains the common variables
 * and methods among the users and is the point at which the factory pattern
 * implements its concrete User
 * @author M_Davies
 */
public abstract class User{
    private String id = "";
    private String forename = "";
    private String surname = "";
    private String address = "";
    private String type = "";

    

    /**
     * Constructor to initialise the class.
     */
    public User() {
        
    }

    /**
     * Accessor to retrieve the unique id of the user. Used for multiple purposes
     * across the program including account deletion, prescription and appointment
     * assignment as well as logging in
     * @return id Unique id of the current user
     */
    public String getId() {
        return id;
    }

    /**
     * Accessor to set the id of a user. This is featured when creating a new 
     * user and is implemented by the Administrator, Secretary and User classes d
     * during account creation
     * @param id Unique id that the new user will have
     */
    public void setId(String id) {
        if (id != null && !id.isEmpty()) 
            this.id = id;
    }

    /**
     * Accessor to retrieve the forename of the user. Used when determining 
     * the name of the current user and also concatenated with the surname in the
     * Doctor view when writing a prescription
     * @return forename Forename of the user
     */
    public String getForename() {
        return forename;
    }

    /**
     * Accessor to set the forename of the user. Used when creating new accounts.
     * Includes validation checking for null or empty values (each user must
     * have a forename).
     * @param forename Forename of the new user
     */
    public void setForename(String forename) {
        if (forename != null && !forename.isEmpty())
            this.forename = forename;
    }

    /**
     * Accessor to retrieve the surname of the user. Used when determining 
     * the name of the current user and also concatenated with the forename in 
     * the Doctor view when writing a prescription 
     * @return surname Surname of the user
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Accessor to set the surname of the user. Used when creating new accounts.
     * Includes validation checking for null or empty values (each user must
     * have a surname).
     * @param surname Surname of the new user
     */
    public void setSurname(String surname) {
        if (surname != null && !surname.isEmpty())
            this.surname = surname;
    }

    /**
     * Accessor to retrieve the address of the user. Used when determining the
     * address of the current user (only patient accounts have unique address, 
     * all others use the surgery address) during prescription writing. 
     * @return address Address of the user
     */
    public String getAddress() {
        return address;
    }

    /**
     * Accessor to set the address of the user. Used when creating new 
     * accounts. Includes validation checking for null and empty values (each 
     * user must have an address).
     * @param address New address of the user
     */
    public void setAddress(String address) {
        if (address != null && !address.isEmpty())
            this.address = address;
    }

    /**
     * Accessor to retrieve the type of the current account. This is used in
     * multiple methods when the amount of functionality the current user has 
     * needs to be determined. The type can be "P" (Patient), "S" (Secretary), 
     * "D" (Doctor) or "A" (Administrator).
     * @return type Type of account the user is
     */
    public String getType() {
        return type;
    }

    /**
     * Accessor to retrieve the type of the current account. Used when creating
     * new accounts. The type must be one of the values above and cannot be 
     * null or empty
     * @param type
     */
    public void setType(String type) {
        if (type != null && !type.isEmpty())      
            this.type = type;
    }
}
