/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FactoryModel;

import ObserverModel.Observable;
import ObserverModel.Observer;
import java.util.Date;

/**
 * Contains the user specific methods and variables for the Administrator
 * <p>
 * Crucial for separating the intended functionality between users
 * </p>
 * @author M_Davies
 */
public class Administrator extends User implements Observer, Observable{
    private String feedback = "None today!";
    private Observable observable;
        
    /**
     * Constructor to initialise the class type. It sets the type
     * of the class. I chose to implement this in the constructor
     * rather than an override method since the class isn't fully completed yet.
     */
    public Administrator() {
        setType("A");
    }

    /**
     * Accessor to retrieve the feedback string, it reverts
     * to the default instance above if a new one isn't set
     * @return feedback Feedback message to the doctor
     */
    public String getFeedback() {
        return feedback;
    }

    /**
     * Accessor to set the feedback string.
     * Note, it does not have to be included since a message is not compulsory
     * @param feedback New feedback message to the doctor
     */
    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
    
    /**
     * Creates a doctor or secretary account based on the type. Used by the
     * administrator to quickly add new accounts
     * @param type The type of account to add 
     */
    public void addAccount(String type){

    }
    
    /**
     * Removes a doctor or secretary account based on the type of the account
     * and the relevant id that corresponds to that specific account 
     * @param type The type of account to delete
     * @param id The specific user id to delete
     */
    public void removeAccount(String type, String id){
        //Compilation
    }
    
    /**
     * Based on the comments shown to the administrator,
     * a feedback message is created and sent to the doctor's account
     * @param feedback Message created by the user to send to the doctor
     */
    public void sendFeedback(String feedback){
        //Compilation
    }

    @Override
    public void updateDetails(String id, String forename, String surname, String address, String gender, Integer age, String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateStock(String medicine, Integer qty) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateAppointment(String id, Date dateOfAppointment) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registerObserver(Observer observer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeObserver(Observer observer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void notifyObserver() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
