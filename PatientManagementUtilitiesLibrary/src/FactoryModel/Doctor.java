/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FactoryModel;

import ObserverModel.Observable;
import ObserverModel.Observer;
import java.util.Date;

/**
 * Contains the user specific methods and variables for the Doctor
 * <p>
 * Crucial for separating the intended functionality between users
 * </p>
 * @author M_Davies
 */
public class Doctor extends User implements Observer, Observable{
    //Variables to create appointment
    private String id = getId();
    private Date dateOfAppointment = new Date();
    
    //Unique variables
    private String medicine = "Unknown";
    private Observable observable;
    
    //Variables for identifying the observer and subject
    private Observable appointment; 
    private static Integer observerIDTracker = 0;
    private Integer observerID;

    /**
     * Constructor to initialise the class subjects, ID. It sets the type
     * of the class (implemented in the constructor over a override method as 
     * the class isn't complete). It also sets the doctor an observer id and
     * as an observer to the appointment class (not using the user id since
     * incrementing that would be unwise)
     * @param appointment Subject the user wishes to observe
     */
    public Doctor(Observable appointment) {
        this.appointment = appointment;
        this.observerID = ++observerIDTracker;
        appointment.registerObserver(this);
        
        setType("D"); 
    }

    /**
     * Accessor to retrieve the medicine name
     * @return medicine Medicine to be used in prescriptions
     */
    public String getMedicine() {
        return medicine;
    }

    /**
     * Accessor to set the medicine. Used when creating new prescriptions and 
     * medicines. No input validation as appointments can be completed without
     * prescribing medicines
     * @param medicine Medicine to prescribe or create
     */
    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }
    
    /**
     * Method to view the records of the current patient. 
     * @param id Unique id of the patient
     */
    public void viewRecords(String id){

    }
    
    /**
     * Method to create a new appointment with the doctor after the current one
     * has been resolved. There will be some validation needed to ensure the 
     * new appointment is set after the current appointment
     * @param dateOfAppointment Date of the new appointment to add
     */
    public void newAppointment(Date dateOfAppointment){

   }
   
    /**
     * Method to create a new medicine and request that the medicine is brought
     * into stock. The method would take the new medicine and update the secretary
     * since the doctor is an observable
     */
    public void createMedicine(){

   }
   
    /**
     * Method for writing appointment notes and a prescription for the patient
     * to collect from the secretary. The prescription is created and passed to
     * the prescription class to be implemented.
     * <p>
     * Because the secretary is an observer of the prescription class, 
     * they should be notified when this method is fired.
     * </p>
     * @param medicine Medicine name to prescribe 
     */
    public void createPrescription(String medicine){

   }

    @Override
    public void updateDetails(String id, String forename, String surname, String address, String gender, Integer age, String type) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void updateStock(String medicine, Integer qty) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    /**
     * Creates a new appointment as a follow up from the previous one. 
     * The doctor reviews their timetable and sets up a new appointment with
     * themselves conforming to the patient's availability.
     * @param id same doctor as the one taking the appointment
     * @param dateOfAppointment new appointment date
     */
    @Override
    public void updateAppointment(String id, Date dateOfAppointment) {
        this.id = id;
        this.dateOfAppointment = dateOfAppointment;
        
        //Sets up a new appointment using the secretaries doctor id and date
        notifyObserver(); //Inform patient and doctor
    }

    @Override
    public void registerObserver(Observer observer) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public void removeObserver(Observer observer) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public void notifyObserver() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
}
