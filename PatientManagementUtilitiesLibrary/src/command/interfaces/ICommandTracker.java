/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.interfaces;

/**
 * Associated to the ICommand interface. Adds functionality to execute the commands,
 * undo or redo the last command to be executed and flags whether a command can be 
 * undone or redone.
 * @author M_Davies
 */
public interface ICommandTracker extends ICommand {
    public Boolean executeCommand(ICommand objACommand);
    public Boolean undoLastCommand();
    public Boolean redoLastCommand();
    public Boolean isUndoable();       
    public Boolean isRedoable();       
}
