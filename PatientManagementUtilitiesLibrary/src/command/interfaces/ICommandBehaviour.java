/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.interfaces;

/**
 * Abstract methods to choose the command to execute. Added functions to pick the
 * command and undo the command if an incorrect choice was made.
 * @author M_Davies
 */
public interface ICommandBehaviour {
    public Boolean doCommand();
    public Boolean undoCommand();
}
