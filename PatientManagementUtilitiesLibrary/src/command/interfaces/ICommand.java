/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command.interfaces;

/**
 * Decorator to the ICommandBehaviour object. Added functions support tracking 
 * of whether the the command has been executed or undone after execution.
 * @author M_Davies
 */
public interface ICommand extends ICommandBehaviour {
    public Boolean isExecuted();
    public Boolean setExecuted(Boolean flag);
    public Boolean isUndone();
}
