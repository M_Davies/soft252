/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandtracker;

import command.interfaces.ICommand;
import command.interfaces.ICommandBehaviour;

/**
 * Concrete implementation of ICommand interface. Takes an ICommandBehaviour and
 * decorates it with the functionality to track its status as executed or undone
 * @author M_Davies
 */
public class Command implements ICommand {
    private ICommandBehaviour delegate;
    private Boolean blnExecuted = false;

    public Command(ICommandBehaviour objACommand) {
        this.delegate = objACommand;
    }

    @Override
    public Boolean isExecuted() {
        return this.blnExecuted;
    }

    @Override
    public Boolean setExecuted(Boolean flag) {
        Boolean blnResult = false;
        if (null != flag) {
            this.blnExecuted = flag;
            blnResult = flag;            
        }
        return blnResult;
    }

    @Override
    public Boolean isUndone() {
        return !this.blnExecuted;
    }

    @Override
    public Boolean doCommand() {
        Boolean done = false;
        done = this.delegate.doCommand();
        this.blnExecuted = done;
        return done;
    }

    @Override
    public Boolean undoCommand() {
        Boolean undone = false;
        undone = this.delegate.undoCommand();
        this.blnExecuted = !undone;
        return undone;
    }
    
}
