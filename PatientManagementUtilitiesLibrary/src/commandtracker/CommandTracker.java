/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commandtracker;

import command.interfaces.ICommand;
import command.interfaces.ICommandTracker;
import java.util.Stack;

/**
 * Provides execution and tracking services for ICommand objects
 * @author M_Davies
 */
public class CommandTracker implements ICommandTracker{

    //Declare stacks of commands
    private Stack<ICommand> stkDone = new Stack<>();
    private Stack<ICommand> stkUndone = new Stack<>();
    
    @Override
    public Boolean executeCommand(ICommand objACommand) {
        Boolean blnExecuted = false;
        if (null != objACommand){
            if (objACommand.doCommand()) {
                this.stkDone.push(objACommand);
                blnExecuted = true;
                }
        }
        return blnExecuted;
    }

    @Override
    public Boolean undoLastCommand() {
        Boolean blnUndone = false;
        if (this.isUndoable()) {
            //Get last command
            ICommand lastCommand = this.stkDone.pop();
            //Undo it
            if (lastCommand.undoCommand()) {
                //Push to undone stack
                this.stkUndone.push(lastCommand);
                blnUndone = true;               
            }
        }
        return blnUndone;
    }

    @Override
    public Boolean redoLastCommand() {
        Boolean blnDone = false;
        if (this.isRedoable()) {
            //Get last undone command
            ICommand lastCommand = this.stkUndone.pop();
            //Redo it
            if (lastCommand.doCommand()) {
                //Push to done stack
                this.stkDone.push(lastCommand);
                blnDone = true;               
            }
        }
        return blnDone;
    }

    @Override
    public Boolean isUndoable() {
        Boolean blnCanUndo = false;
        //If commands are on done stack, can undo last command
        if (!this.stkDone.empty()) {
            blnCanUndo = true;            
        }
        return blnCanUndo;
    }

    @Override
    public Boolean isRedoable() {
        Boolean blnCanRedo = false;
        //If commands are on undone stack, can redo last command
        if (!this.stkUndone.empty()) {
            blnCanRedo = true;            
        }
        return blnCanRedo;
    }   

    @Override
    public Boolean isExecuted() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Boolean setExecuted(Boolean flag) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Boolean isUndone() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Boolean doCommand() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Boolean undoCommand() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
}
