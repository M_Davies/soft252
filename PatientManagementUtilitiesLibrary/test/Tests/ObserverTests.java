/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tests;

import ObserverModel.*;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * My unit tests for the accessor and concrete methods across the the factory 
 * pattern aspects of the program.
 * Only one test is implemented for each accessor for brevity reasons.
 * @author M_Davies
 */
public class ObserverTests {
    
    public ObserverTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * My first and only test on the appointment class, I realised that I wouldn't
     * be able to achieve a instantiation of the appointment or stock classes
     * since they were too isolated from the Doctor and Secretary classes. They
     * need to be directly connected via aggregation or a controller interface.
     */
    @Test
    public void testDoctorName(){
        Prescription doctorName = new Appointment() {
            @Override
            public void updateDetails(String id, String forename, String surname, 
                    String address, String gender, Integer age, String type) {
                throw new UnsupportedOperationException("Not supported yet."); 
            }

            @Override
            public void updateStock(String medicine, Integer qty) {
                throw new UnsupportedOperationException("Not supported yet."); 
            }

            @Override
            public void updateAppointment(String id, Date dateOfAppointment) {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        };
    }
    
    @Test
    public void testMedicineName(){
       // Stock medicineStock = new Stock();
    }
}
