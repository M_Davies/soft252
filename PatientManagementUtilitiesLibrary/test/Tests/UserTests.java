package Tests;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import FactoryModel.*;
import ObserverModel.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * My unit tests for the accessor and concrete methods across the the factory 
 * pattern aspects of the program.
 * Only one test is implemented for each accessor for brevity reasons.
 * @author M_Davies
 */
public class UserTests {
    private Observable observable;
    
    public UserTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Administrator test checking that if an invalid value is entered, a console
     * message is returned detailing the error. Test is failed as the returned 
     * value is the object type, not the console message.
     */
    @Test
    public void testaddAccount(){
        //Testing to see if the compiler will reject an unknown user type
        Administrator newAccount = new Administrator();
        Administrator newUser = new Administrator();
        newUser.addAccount("Z");
        assertEquals("Invalid user type", newUser);
        
        //Testing to see if the compiler will accept an known user type
        //String type = "A";
        //Administrator newAccount = new Administrator();
        //User newUser = newAccount.addAccount(type);
        //assertEquals("Invalid user type", newUser);
    }
    
    /**
     * Accessor tests to check the correct values are returned. Test returned null
     * with a java.lang.NullPointException error. I suspect this is due to my 
     * code being too heavily coupled within the observer pattern. So that even when
     * testing, a physical observer is required by the compiler to create the object
     */
    @Test
    public void testId(){
        //Create patient instance and observer to fill the gap
        Observable observer = null;
        User newUser = new Patient(observer);
        
        //Create valid string and test its return value
        String id = "A1098";
        newUser.setId(id);
        assertEquals(id, newUser.getId());
    }
    
    /**
     * Accessor tests to check the correct values are returned. 2nd test passed
     * using the administrator class this time. This is due to the administrator 
     * not instantiating the observable interface in its constructor, so no null
     * value is passed to the class and the program runs effectively. This proves 
     * the User class accessor is functional however
     */
    @Test
    public void testForename(){
        //Create Administrator instance
        User newUser = new Administrator();
        
        //Create valid string and test its return value
        String forename = "James";
        newUser.setForename(forename);
        assertEquals(forename, newUser.getForename());
    }
    
    /**
     * This test also failed with the same error as the 2nd test, confirming my 
     * theory that it's the observable interface causing the issue. My first idea
     * of a fix would just have these classes implement the observer interface and 
     * use a factory controller to produce concrete implementations of the 
     * observable class
     * 
     */
    @Test
    public void testSurname(){
        //Create Doctor instance
        User newUser = new Doctor(null);
        
        //Create valid string and test its return value
        String surname = "Adams";
        newUser.setSurname(surname);
        assertEquals(surname, newUser.getSurname());
    }
    
    /**
     * Final test to ensure testForename was not a one off event. This test also
     * passed, confirming that the administrator accessors work correctly
     */
    @Test
    public void testAddress(){
        //Create Administrator instance
        User newUser = new Administrator();
        
        //Create valid string and test its return value
        String address = "91 Fake Street";
        newUser.setAddress(address);
        assertEquals(address, newUser.getAddress());
    }
}
