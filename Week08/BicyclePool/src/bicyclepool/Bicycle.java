/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool;

import bicyclepool.Decorator.BicycleDecorator;
import java.io.*;

/**
 *
 * @author toxic
 */
public abstract class Bicycle implements Serializable{

    //Variables set to protected so decorator can access them
    protected transient String type = "UNKNOWN";
    protected transient Integer id = 0000;
    protected Integer pool = 15;
    
    //Constructor
    public Bicycle() {
    }
    
    //Abstract method to set the cost
    public abstract Double cost();
    
    //Getters.
    public String getType() {
        return type;
    }

    public Integer getId() {
        return id;
    }

    public Integer getPool() {
        return pool;
    }
    
    //Setters.
    public void setType(String type){
        if (type != null && !type.isEmpty()) {
            this.type = type;
        }
    }
    
    public void setId(Integer id){
        if (id != null && id > 0) {
            this.id = id;
        }
    }
    
    public void setPool(Integer pool){
        if (pool != null && pool > 0) {
            this.pool = pool;
        }
        else{
            System.out.println("Pool is empty!");
        }
    }
}
