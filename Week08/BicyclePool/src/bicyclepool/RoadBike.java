/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool;

/**
 *
 * @author mdavies16
 */
public abstract class RoadBike extends Bicycle {
    //Displays bike type
    public RoadBike() {
        this.type = "Road Bike ";
    }
    
    //Specifies cost
    @Override
    public Double cost(){
        return 10.00;
    }
}
