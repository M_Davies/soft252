/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool.Decorator;

import bicyclepool.Bicycle;

/**
 *
 * @author toxic
 */
public abstract class BackLight extends BicycleDecorator {
    //Constructor (extending superclass)
    public BackLight(Bicycle bicycle) {
        super(bicycle);
    }
    
    //Overrides for output and total cost
    @Override
    public String getType(){
        return bicycle.getType() + " Back light ";
    }
    
    @Override
    public Double cost(){
        return bicycle.cost() + 1.25;
    }
}
