/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool.Decorator;

import bicyclepool.Bicycle;

/**
 *
 * @author toxic
 */
public abstract class Helmet extends BicycleDecorator{
    //Constructor (extending superclass)
    public Helmet(Bicycle bicycle) {
        super(bicycle);
    }
    
    //Overrides for output and total cost
    @Override
    public String getType(){
        return bicycle.getType() + " Helmet ";
    }
    
    @Override
    public Double cost(){
        return bicycle.cost() + 1.50;
    }
}
