/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool.Decorator;
import bicyclepool.Bicycle;

/**
 *
 * @author toxic
 */
public abstract class BicycleDecorator extends Bicycle {
    //Create bicycle object and set penultimate variables
    Bicycle bicycle;
    
    public BicycleDecorator(Bicycle bicycle){
        this.bicycle = bicycle; 
    }
}
