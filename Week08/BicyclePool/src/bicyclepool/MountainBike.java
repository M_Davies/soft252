/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool;

/**
 *
 * @author mdavies16
 */
public abstract class MountainBike extends Bicycle {
    //Displays bike type
    public MountainBike() {
        this.type = "Mountain Bike ";
    }
    
    //Specifies cost
    @Override
    public Double cost(){
        return 10.00;
    }
}
