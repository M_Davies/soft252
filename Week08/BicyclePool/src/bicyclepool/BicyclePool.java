/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bicyclepool;
import bicyclepool.Decorator.*;
import java.util.*;
import utilities.Serialiser;
/**
 *
 * @author toxic
 */
public class BicyclePool {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Variables and objects
        Scanner sc = new Scanner(System.in);       
        
        //User chooses type of bike
        System.out.println("Welcome to the CREATIONAL RIDING AND PEDALLING system\n"
        + "Otherwise known as C.R.A.P!\n");        
       
        System.out.println("Please choose the type of bicycle you would like to hire out today\n" +
                "Mountain\nRoad\nFolding\nTouring");        
                
        switch (sc.next().toUpperCase()) {
            case "MOUNTAIN":
                {
                    //Create object and serialiser
                    Bicycle bike = new MountainBike() {};                                      
                    
                    //Take employee number and store local copy for ref later
                    System.out.println("Employee number?");
                    bike.setId(sc.nextInt());
                    Integer id = bike.getId();
                                        
                    //Additional items
                    System.out.println("Would you like any additional items?\nList the index number"
                            + " for each that apply");
                    System.out.println("0. None\n1. Helmet\n2. Front Light\n3. Back Light\n4. Hand Pump");
                    //Run through input and check for extras
                    String extras = sc.next();
                    System.out.println(extras);                                        
                    
                    //STANDALONE BIKE
                    if (extras.contains("0") && extras.length() == 1) {
                        Output(bike, bike.getId(),bike.getType(), bike.cost());
                    }
                    //0 INCLUDED IN A STANDARD INPUT
                    else if (extras.contains("0")) {
                        Error();
                    }
                    
                    //HELMET
                    if (extras.contains("1") && !extras.contains("0")) {
                        bike = new Helmet(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //FRONT LIGHT
                    if (extras.contains("2") && !extras.contains("0")) {
                        bike = new FrontLight(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //BACK LIGHT
                    if (extras.contains("3") && !extras.contains("0")){
                        bike = new BackLight(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //HAND PUMP
                    if (extras.contains("4") && !extras.contains("0")){
                        bike = new HandPump(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }                 
                    break;
                }
            case "ROAD":
                {
                    //Create object
                    Bicycle bike = new RoadBike() {};
                    //Take employee number and store local copy for ref later
                    System.out.println("Employee number?");
                    bike.setId(sc.nextInt());
                    Integer id = bike.getId();
                    
                    //Additional items
                    System.out.println("Would you like any additional items?\nList the index number"
                            + " for each that apply");
                    System.out.println("0. None\n1. Helmet\n2. Front Light\n3. Back Light\n4. Hand Pump");
                    //Run through input and check for extras
                    String extras = sc.next();
                    System.out.println(extras);
                    
                    //STANDALONE BIKE
                    if (extras.contains("0") && extras.length() == 1) {
                        Output(bike, bike.getId(),bike.getType(), bike.cost());
                    }
                    //0 INCLUDED IN A STANDARD INPUT
                    else if (extras.contains("0")) {
                        Error();
                    }       
                    
                    //HELMET
                    if (extras.contains("1") && !extras.contains("0")) {
                        bike = new Helmet(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //FRONT LIGHT
                    if (extras.contains("2") && !extras.contains("0")) {
                        bike = new FrontLight(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //BACK LIGHT
                    if (extras.contains("3") && !extras.contains("0")){
                        bike = new BackLight(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //HAND PUMP
                    if (extras.contains("4") && !extras.contains("0")){
                        bike = new HandPump(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }
                    break;
                }
            case "FOLDING":
                {
                    //Create object
                    Bicycle bike = new FoldingBike() {};
                    //Take employee number and store local copy for ref later
                    System.out.println("Employee number?");
                    bike.setId(sc.nextInt());
                    Integer id = bike.getId();
                    
                    //Additional items
                    System.out.println("Would you like any additional items?\nList the index number"
                            + " for each that apply");
                    System.out.println("0. None\n1. Helmet\n2. Front Light\n3. Back Light\n4. Hand Pump");
                    //Run through input and check for extras
                    String extras = sc.next();
                    System.out.println(extras);
                    
                    //STANDALONE BIKE
                    if (extras.contains("0") && extras.length() == 1) {
                        Output(bike, bike.getId(),bike.getType(), bike.cost());
                    }
                    //0 INCLUDED IN A STANDARD INPUT
                    else if (extras.contains("0")) {
                        Error();
                    }       
                
                    //HELMET
                    if (extras.contains("1") && !extras.contains("0")) {
                        bike = new Helmet(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //FRONT LIGHT
                    if (extras.contains("2") && !extras.contains("0")) {
                        bike = new FrontLight(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //BACK LIGHT
                    if (extras.contains("3") && !extras.contains("0")){
                        bike = new BackLight(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //HAND PUMP
                    if (extras.contains("4") && !extras.contains("0")){
                        bike = new HandPump(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }
                    break;
                }
            case "TOURING":
                {
                    //Create object
                    Bicycle bike = new TouringBike() {};
                    //Take employee number and store local copy for ref later
                    System.out.println("Employee number?");
                    bike.setId(sc.nextInt());
                    Integer id = bike.getId();
                    
                    //Additional items
                    System.out.println("Would you like any additional items?\nList the index number"
                            + " for each that apply");
                    System.out.println("0. None\n1. Helmet\n2. Front Light\n3. Back Light\n4. Hand Pump");
                    //Run through input and check for extras
                    String extras = sc.next();
                    
                    //STANDALONE BIKE
                    if (extras.contains("0") && extras.length() == 1) {
                        Output(bike, bike.getId(),bike.getType(), bike.cost());
                    }
                    //0 INCLUDED IN A STANDARD INPUT
                    else if (extras.contains("0")) {
                        Error();
                    }       

                    //HELMET
                    if (extras.contains("1") && !extras.contains("0")) {
                        bike = new Helmet(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //FRONT LIGHT
                    if (extras.contains("2") && !extras.contains("0")) {
                        bike = new FrontLight(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //BACK LIGHT
                    if (extras.contains("3") && !extras.contains("0")){
                        bike = new BackLight(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }       //HAND PUMP
                    if (extras.contains("4") && !extras.contains("0")){
                        bike = new HandPump(bike) {};
                        Output(bike, id, bike.getType(), bike.cost());
                    }
                    break;
                }
            default:
                //Invalid input - shutdown system
                System.out.println("Thats not a bike!\nConnection Closing...");
                System.exit(0);
        }
    }
    
    private static void Output(Bicycle bike, Integer id, String type, Double cost){
        //Output monthly cost
        System.out.println("Employee: " + id + ". You ordered a " + type + ". Including extras, this will cost you £" + cost + " a month.");
        
        //Declare scanner and serialiser for reading/writing
        Scanner sc = new Scanner(System.in);
        Serialiser serialiser = new Serialiser("bicycle_pool.ser");
                
        //Remove bike from pool
        bike.setPool(bike.getPool() - 1);
        System.out.println("There are " + bike.getPool() + " bikes left!");
                
        //Add bike to pool
        serialiser.writeObject(bike);
        
        //Check bike pool count
        bike = (Bicycle) serialiser.readObject();
        if (bike.getPool() < 1){
            System.out.println("There are no more bikes left in the pool! Would you like to replenish some? (Y/N)");
                switch(sc.next().toUpperCase()){
                    case "Y":
                        {
                            System.out.println("What is the new pool size?");
                                try
                                {
                                    bike.setPool(sc.nextInt());
                                    System.out.println("New pool size set to " + bike.getPool());
                                } 
                                catch(Exception f)
                                {
                                    System.out.println("ERROR - Not an integer!");                                   
                                }
                            break;
                        }
                    case "N":
                        {
                            System.out.println("No bikes added. Exiting...");
                            System.exit(0);
                            break;
                        }
                    default:
                        System.out.println("ERROR - Please type Y or N");
                        break;
                }
        }                
    }
    private static void Error(){
        System.out.println("INVALID CHOICES! - Have you included a 0 in your input?");
        System.exit(0);
    }
}
