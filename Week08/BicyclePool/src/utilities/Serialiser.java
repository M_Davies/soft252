/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.*;

/**
 *
 * @author mdavies16
 */
public class Serialiser {
    //Properties
    private String name;
    
    //Constructer
    public Serialiser(String filename){
        name = filename;
    }
    
    ////Accessors
    public void setName(String filename){
        name = filename;
    }
    
    public String getName(){
        return name;
    }
    
    //Serilisation Read
    public Serializable readObject(){
        Serializable loadedObject = null;
        try {
         FileInputStream fileIn = new FileInputStream(name);
         ObjectInputStream in = new ObjectInputStream(fileIn);
         loadedObject = (Serializable) in.readObject();
         in.close();
         fileIn.close();
         System.out.println("Data loaded from: "+ name);
        } catch (IOException i) {
            System.out.println("File not found.");
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
        }
        return loadedObject;
    }
    
    //Serilisation Write
    public boolean writeObject(Serializable object){
        try {
            FileOutputStream fileOut = new FileOutputStream(name);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(object);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved in: " + name);
            return true;
         } catch (IOException i) {
            System.out.println("Failed to load!");
            i.printStackTrace();
            return false;
         }
    }
}
