/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classification;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mdavies16
 */
public class StageResultsTest {
    private StageResults empty;   //0 credits and marks.  
    private StageResults full;    //120 credits and marks.  
    private StageResults halFull; //60 credits and some marks.
    
    public StageResultsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        empty = new StageResults();
        
        //full - 120 credits worth but no initial stage2Average.
        full = new StageResults();
        full.addModuleMark(120, 50.0);
        
        //halfFull - 60 credits worth but no initial stage2Average
        halFull = new StageResults();
        halFull.addModuleMark(60, 50.0);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetStage2Average() {
    }

    @Test
    public void testGetTotalCredits() {
    }

    @Test
    public void testGetTotalMarks() {
    }

    @Test
    public void testSetStage2Average() {
    }

    @Test
    public void testIsComplete() {
        System.out.println("Testing is Complete");
        
        //Check empty object isn't complete.
        assertFalse("empty object", empty.isComplete());
        
        //Check half full object is complete.
        assertFalse("half full object", halFull.isComplete());
        
        //Check full object is complete.
        assertTrue("full object", full.isComplete());
    }

    @Test
    public void testResetValues() {
        System.out.println("Testing resetValues");
        
        //Set state of full object to 0's.
        full.resetValues();
        
        //Set expected results.
        int expIntResult = 0;
        double expDoubleResult = 0.0;
        
        //Check attributes to test that reset worked.
        assertEquals("credits", expIntResult, full.getTotalCredits());
        assertEquals("total", expDoubleResult, full.getTotalMarks(), 0.0);
        
        //Full object back to original state.
        full.addModuleMark(120, 50.0);
    }

    @Test
    public void testAddModuleMark() {
        System.out.println("Testing addModuleMark");
        
        //Set expected results.
        int expIntResult = 0;
        double expDoubleResult = 0.0;
        
        //Check attributes to test that reset worked.
        assertEquals("credits", expIntResult, empty.getTotalCredits());
        assertEquals("total", expDoubleResult, empty.getTotalMarks(), 0.0);
                
        //Restore object to original state.
        empty.resetValues();
    }

    @Test
    public void testCalculateAverageSoFar() {
        System.out.println("Testing calculateAverageSoFar");
        
        //Test with 0 credits and marks.
        assertEquals("empty", 0.0, empty.calculateAverageSoFar(), 0.0);
        
        //Test with 120 credits at 50%
        assertEquals("full @ 50%", 50.0, full.calculateAverageSoFar(), 0.0);
        
        //Test with 120 credits at 100%
        full.resetValues();
        full.addModuleMark(120,100.0);
        assertEquals("full @ 100%", 100.0, full.calculateAverageSoFar(), 0.0);
        
        //Test with 120 credits at 43.92%
        full.resetValues();
        full.addModuleMark(120, 43.92);
        assertEquals("full @ 43.92%", 43.92, full.calculateAverageSoFar(), 0.0);
        full.resetValues();
        full.addModuleMark(120, 50.0);
        
        //Test with 60 credits at 50%
        assertEquals("halFull @ 50%", 50.0, halFull.calculateAverageSoFar(), 0.0);
        
        //Test with 60 credits at 100%
        halFull.resetValues();
        halFull.addModuleMark(60, 100.0);
        assertEquals("halFull @ 100%", 100.0, halFull.calculateAverageSoFar(), 0.0);
        
        //Test with 60 credits at 64.77%
        halFull.resetValues();
        halFull.addModuleMark(60, 64.77);
        assertEquals("halFull @ 64.77%", 64.77, halFull.calculateAverageSoFar(), 0.0);
        halFull.resetValues();
        halFull.addModuleMark(60, 50.0);
    }

    @Test
    public void testPredictClass() {
        System.out.println("Testing predictClass");
        
        // Array to hold the stage 3 marks
        double[] marks = {0.00, 50.00, 50.00, 100.00, 39.99, 40.0,
            49.99, 50.0, 59.99, 60.0, 69.99, 70.0, 99.99, 35.67,
            44.22, 56.39, 64.00, 76.80};
        
        // Array of corresponding classifications with no stage 2 marks
        String[] expResult1 = {"No marks!", "Lower 2nd",
            "Lower 2nd", "1st", "FAIL", "3rd", "3rd", "Lower 2nd",
            "Lower 2nd", "Upper 2nd", "Upper 2nd", "1st", "1st",
            "FAIL", "3rd", "Lower 2nd", "Upper 2nd", "1st"};
        
        // Array to hold the stage 2 marks
        double[] stage2 = {0.0, 50.0, 56.77, 70.00, 56.00, 49.53,
            52.78, 64.89, 47.82, 89.22, 70.00, 48.56, 100.00, 38.67,
            56.29, 77.00, 65.00, 83.00};
        // Array of corresponding classifications where there are stage 2 marks
        String[] expResult2 = {"No marks!", "Lower 2nd", "Lower 2nd",
            "1st",  "Lower 2nd", "3rd", "Lower 2nd", "Upper 2nd", "3rd",
            "1st", "1st", "3rd", "1st", "FAIL", "Lower 2nd",
            "1st", "Upper 2nd", "1st"};
        
        // Run tests with no stage 2 average
        for (int count = 0; count < marks.length; count++) {
            full.resetValues();
            full.addModuleMark(120, marks[count]);
            assertEquals("120 credits, mark = " + marks[count], expResult1[count],
                full.predictClass());
        }
       // Run tests that include stage 2
       for(int count = 0; count < stage2.length; count++){
           full.resetValues();
           full.addModuleMark(120, stage2[count]);
           full.setStage2Average(stage2[count]);
           assertEquals("120 credits mark = " + stage2[count] + " stage 2 = " + 
                   stage2[count], expResult2[count], full.predictClass());
       }
       
       // Reset the object to standard
        full.resetValues();
        full.addModuleMark(120, 50.0);
        
        // Final check that no predcition is returned if the number of
        // credits is less than 120.
        assertEquals("No prediction for 60, i.e. < 120 credits", 
                "Insufficient credits", halFull.predictClass());
        assertEquals("No prediction for 0, i.e. < 120 credits", 
                "Insufficient credits", empty.predictClass());
    }
    
    @Test
    public void testFullOperation(){
        int[] credits = {10, 10, 10, 20, 20, 40, 10};
        double[] marks = {60.6, 44.45, 80.0, 56.99, 62.3, 68.4, 59.11};
        double stage2 = 61.2;
        
        StageResults finalTest = new StageResults();
        
        //Add module marks and set stage2 average.
        for (int count = 0; count < credits.length; count++){
            finalTest.addModuleMark(credits[count], marks[count]);            
        }
        finalTest.setStage2Average(stage2);
        
        //Test it.
        assertEquals("stage 3 average", 63.03, 
                finalTest.calculateAverageSoFar(), 0.0);
        assertEquals("predicted class", "Upper 2nd", 
                finalTest.predictClass());            
    }
}
