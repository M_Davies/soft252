/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import GUIView.Login;
import FactoryModel.UserFactory;

/**
 * The main method of the program, this is where the view and model classes are 
 * called up and ready for user interaction
 * @author M_Davies
 */
public class PatientManagement {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Instantiate the model
        UserFactory userFactory = new UserFactory();
         
        //Instantiate the view
        Login login = new Login();
        login.setVisible(true);        
    }
    
}
