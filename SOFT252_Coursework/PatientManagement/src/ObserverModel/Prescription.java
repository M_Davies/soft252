package ObserverModel;



import FactoryModel.User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Prescription is a subclass of the doctor class, used when creating new 
 * prescriptions. Containing no methods, the class is used as a baseplate for 
 * the doctor class when creating prescriptions
 * @author M_Davies
 */
public abstract class Prescription extends User {
    private String patientName = "";
    private String patientAddress = "";
    private String gender = "";
    private Integer age = 0;
    private String doctorName = "";
    private String doctorAddress = "";
    private String notes = "";
    private String medicine = "";
    private Integer qty = 0;
    private String dosage = "";

    /**
     * Constructor to initialise the class as an observable so the doctor is 
     * alerted when its complete.
     */
    public Prescription() {    
    }

    /**
     * Accessor to retrieve the patient name. Returns the forename and surname
     * of the patient, concatenated together. This is so it matches the format
     * of the prescription.
     * @return fullPatient Full name of the patient
     */
    public String getPatientName() {
        String fullPatient = getForename() + getSurname();
        return fullPatient;
    }

    /**
     * Accessor to set the patient name. Includes validation checking for null
     * and empty values (a patient must always have a name). If valid, the 
     * setter retrieves the patient name and concatenates the surname on the back
     * @param patientName New full name of the patient
     */
    public void setPatientName(String patientName) {
        if (patientName != null && !patientName.isEmpty()) {
            this.patientName = patientName + getSurname();
        }
    }

    /**
     * Accessor to retrieve the patient's address. Returns the address
     * of the patient so the prescription can be filled out.
     * @return patientAddress Address of the patient
     */
    public String getPatientAddress() {
        return patientAddress;
    }

    /**
     * Accessor to set the address of the patient. Includes validation checking 
     * for null and empty values (a patient must have an address).
     * @param patientAddress New patient address
     */
    public void setPatientAddress(String patientAddress) {
        if(patientAddress != null && !patientAddress.isEmpty())
            this.patientAddress = patientAddress;
    }

    /**
     * Accessor to retrieve the gender of the patient. Returns the gender of the
     * patient so the prescription can be filled out.
     * @return gender Gender of the patient
     */
    public String getGender() {
        return gender;
    }

    /**
     * Accessor to set the gender of the patient. Includes validation checking for
     * null and empty values (a patient must have a gender but it doesn't have to
     * be male or female)
     * @param gender New gender of the patient
     */
    public void setGender(String gender) {
        if(gender != null && !gender.isEmpty())
            this.gender = gender;
    }

    /**
     * Accessor to retrieve the age of the patient. Returns the age of the
     * patient so the prescription can be filled out.
     * @return age Age of the patient
     */
    public Integer getAge() {
        return age;
    }

    /**
     * Accessor to set the age of the patient. Includes validation checking for
     * null and negative values (a patient must have an age and cannot be less
     * than 0 for obvious reasons)
     * @param age
     */
    public void setAge(Integer age) {
        if(age != null && age > 0)
            this.age = age;
    }

    /**
     * Accessor to retrieve the doctor's name. Returns the forename and surname
     * of the doctor, concatenated together. This is so it matches the format
     * of the prescription.
     * @return doctorName Full name of the doctor
     */
    public String getDoctorName() {
        String fullDoctor = getForename() + getSurname();
        return fullDoctor;
    }

    /**
     * Accessor to set the doctors name. Includes validation checking for null
     * and empty values (a doctor must always have a name). If valid, the 
     * setter retrieves the doctor's name and concatenates the surname on the back
     * @param doctorName New full name of the doctor
     */
    public void setDoctorName(String doctorName) {
        if(doctorName != null && !doctorName.isEmpty())
            this.doctorName = doctorName + getSurname();
    }

    /**
     * Accessor to retrieve the doctor's address. Returns the address
     * of the doctor (surgery address) so the prescription can be filled out.
     * @return doctorAddress Address of the surgery the doctor is based at
     */
    public String getDoctorAddress() {
        return doctorAddress;
    }

    /**
     * Accesor to set the doctor's address. I'm not sure where this would be used
     * since the surgery address would be coded in the Doctor class by 
     * default but I included it to keep extension options open. Include validation
     * checking for null and empty values (the doctor must list the surgery address)
     * @param doctorAddress New address of the surgery the doctor is based at
     */
    public void setDoctorAddress(String doctorAddress) {
        if(doctorAddress != null && !doctorAddress.isEmpty())
            this.doctorAddress = doctorAddress;
    }

    /**
     * Accessor to retrieve the notes taken by the doctor during the appointment.
     * Used when filling out the prescription
     * @return notes Examination notes written by the doctor
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Accessor to set the examination notes. Includes validation checking for null
     * and empty values (a doctor must always take notes during an appointment 
     * to prove the appointment was legitimate).
     * @param notes New examination notes written by the doctor during the appointment
     */
    public void setNotes(String notes) {
        if(notes != null && notes.isEmpty())
            this.notes = notes;
    }

    /**
     * Accessor to retrieve the medicine prescribed by the doctor. Called by the
     * secretary when the patient hands in the prescription. 
     * @return medicine Medicine prescribed by the doctor
     */
    public String getMedicine() {
        return medicine;
    }

    /**
     * Accessor to set the medicine prescribed by the doctor. Called by the
     * doctor when the they complete the prescription. No validation checks since
     * this will be done at the view stage and a patient may not need to be 
     * prescribed drugs
     * @param medicine New medicine prescribed by the doctor
     */
    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }

    /**
     * Accessor to retrieve the quantity of medicine prescribed by the doctor. 
     * Called by the secretary when the patient hands in the prescription. 
     * @return qty Quantity of the medication that the patient should be given
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * Accessor to set the quantity of the medicine prescribed by the doctor. 
     * Called by the doctor when they complete the prescription. This does include
     * an negative validation check but no null check since there may be no need
     * to supply medication to the patient
     * @param qty New Quantity of the medication that the patient should be given
     */
    public void setQty(Integer qty) {
        if(qty > 0)
            this.qty = qty;
    }

    /**
     * Accessor to retrieve the dosage advice given to the patient when they collect
     * their prescription. Called by the patient when they view their prescription
     * @return dosage Dosage of medicine the patient should take
     */
    public String getDosage() {
        return dosage;
    }

    /**
     * Accessor to set the dosage advice given to the patient. No validation checks 
     * since this will be done at the view stage and a patient may not need to be 
     * prescribed drugs.
     * @param dosage New dosage of medicine the patient should take
     */
    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
    
    
}
