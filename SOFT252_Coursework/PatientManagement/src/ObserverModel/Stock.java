package ObserverModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;

/**
 * This class implements the both the observer and observable interfaces and also
 * inherits the prescription class using weak aggregation.This was done so the 
 * only communication with the class would have to be through the observer
 * pattern and would alert each user involved to a change with the stock.
 * @author M_Davies
 */
public abstract class Stock extends Prescription implements Observer, Observable {
    private String medicine = "";
    private Integer qty = 0;
    private ArrayList stock = new ArrayList();
    private ArrayList <Observer> observers;

    /**
     * Constructor to initialise the class and the arraylist of observers
     */
    public Stock() {
        observers = new ArrayList();
    }

    /**
     * Overrides the medicine value from the prescription to the new name 
     * currently selected in the stock. Used when retrieving the information
     * about currently stocked medicines. No need for a setter method since this 
     * is handled in the doctor class
     * @return medicine Name of the medicine
     */
    @Override
    public String getMedicine() {
        return medicine;
    }
    
    /**
     * Overrides the medicine quantity in stock from the prescription to the new 
     * value currently selected in the stock. Used when retrieving the quantity 
     * of currently stocked medicines. A setter method is needed here since the 
     * secretary will need to alter the quantity of the stock, usually when a 
     * patient collects a prescription
     * @return qty Quantity of the medicine in stock
     */   
    @Override
    public Integer getQty() {
        return qty;
    }

    /**
     * Validation checking for null and negative values is also included here 
     * (it shouldn't be possible to order a medicine without specifying the
     * quantity)
     * @param qty New quantity of the medicine in stock
     */
    @Override
    public void setQty(Integer qty) {
        if(qty != null && qty > 0)
            this.qty = qty;
    }

    /**
     * Accessor to retrieve the arraylist of all the stock items (included here 
     * are the names and quantity amounts of each medicine). Will be implemented
     * by the Secretary when reviewing currently stocked medicines.
     * @return stock List of all the currently stocked medicines and their amount
     * in stock
     */
    public ArrayList getStock() {
        return stock;
    }

    /**
     * Accessor to set the items in the arraylist of stock items. This is used when
     * the secretary orders new stock in, updating the stock to include the new
     * medicine ordered. Includes validation checking to ensure the stock always
     * has medicines in it (a clinic is no use without medicines)
     * @param stock Updated list of the currently stocked medicines plus any
     * newly added medicines
     */
    public void setStock(ArrayList stock) {
        if(stock != null && !stock.isEmpty())
            this.stock = stock;
    }
    
    /**
     * Implementing the abstract method from the observer interface, the method
     * calls the set stock, quantity and medicine methods above. This is so that
     * when a new medicine is added, a current quantity in stock has 
     * changed or when a medicine is out or in stock, the relevant parties are 
     * informed of the change in the stock
     * @param medicine Medicine status to be altered
     * @param qty  Quantity of the medicine to be altered
     */
    @Override
    public void updateStock(String medicine, Integer qty){
        notifyObserver();
    }
    
    /**
     * This method is implemented by the doctor through the observable to fill 
     * out their prescription on the patient. The prescription is stored here 
     * until the patient comes to collect it (seen in the method below). They're
     * informed of the change via the observer interface
     * @param medicine Medicine name to be collected
     * @param qty Medicine quantity to be removed from the stock (also checks
     * if the secretary actually has the medicine in stock)
     * @param dosage Dosage the patient should take of the medicine
     */
    public void prescribeMedicine(String medicine, Integer qty, String dosage){
        
    }
    
    /**
     * This method follows on from the one above. After a patient is notified that,
     * their prescription is ready, this method is called from prescribeMedicine().
     * It removes the quantity of the specified medicine from the secretaries 
     * stock and passes the medicine name and dosage values to the patient. 
     * @param medicine Medicine name to collect
     * @param qty Quantity of the medicine to be removed from the stock
     * @param dosage Dosage the patient should take of the medicine.
     */
    public void collectMedicine(String medicine, Integer qty, String dosage){
        
    }
    
    /**
     * Used to add new relevant users to the subscription list (such as a new 
     * secretary or doctor)
     * @param newObserver New observer to add
     */
    @Override
    public void registerObserver(Observer newObserver){
        observers.add(newObserver);
    }
    
    /**
     * Used to remove observers from the subscription list (e.g. if their account
     * has been deleted or they do not need to be subscribed to updates about the 
     * stock anymore)
     * @param deleteObserver Observer to be deleted
     */
    @Override
    public void removeObserver (Observer deleteObserver){
        Integer observerIndex = observers.indexOf(deleteObserver);
        observers.remove(observerIndex);
    }
    
    /**
     * Sends an update message to all users accounts that need to know the status
     * of the stock (the secretary and occasionally the patient).
     * 
     */
    @Override
    public void notifyObserver(){
        observers.forEach((observer) -> {
            observer.updateStock(medicine, qty);
        });
    }
}
