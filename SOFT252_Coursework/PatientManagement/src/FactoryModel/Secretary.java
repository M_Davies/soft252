/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FactoryModel;

import ObserverModel.Observable;
import ObserverModel.Observer;
import java.util.Date;

/**
 * Contains the user specific methods and variables for the Secretary
 * <p>
 * Crucial for separating the intended functionality between users
 * </p>
 * @author M_Davies
 */
public class Secretary extends User implements Observer, Observable{
    //Variables to create appointment
    private String id = getId();
    private Date dateOfAppointment = new Date();
    
    //Unique Variables
    private String medicine = "";
    private Integer qty = 0;

    //Variables for identifying the observer and subject
    private Observable appointment; 
    private static Integer observerIDTracker = 0;
    private Integer observerID;
    
    /**
     * Constructor to initialise the class subjects, ID. It sets the type
     * of the class (implemented in the constructor over a override method as 
     * the class isn't complete). It also sets the secretary an observer id and
     * as an observer to the appointment class (not using the user id since
     * incrementing that would be unwise)
     * @param appointment Subject the user wishes to observe
     */
    public Secretary(Observable appointment) {
        this.appointment = appointment;
        this.observerID = ++observerIDTracker;
        appointment.registerObserver(this);
        
        setType("S"); 
    }

    /**
     * Accessor to retrieve the medicine name
     * @return medicine Name of the medicine
     */
    public String getMedicine() {
        return medicine;
    }

    /**
     * Accessor to set the medicine name. Used when ordering new medicine types.
     * Includes validation checking for null or empty medicine names (a medicine
     * must always have an identifying name)
     * @param medicine Name of the medicine to be ordered
     */
    public void setMedicine(String medicine) {
        if (medicine != null && !medicine.isEmpty()) {
            this.medicine = medicine;
        }
    }

    /**
     * Accessor to retrieve the quantity of the medicine to be ordered or viewed
     * in the stock list
     * @return qty Quantity of the medicine
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * Accessor to set the quantity of a particular medicine. 
     * Used when ordering new supplies or after a prescription has been collected
     * @param qty Quantity of the medicine in stock or to be ordered
     */
    public void setQty(Integer qty) {
        if (qty != null && qty > -1) {
            this.qty = qty;
        }       
    }
    
    /**
     * Method to approve of a new patient account. 
     * The observer method alerts
     * the secretary of a new request and this method deals with that, either
     * approving or rejecting the request for a new account. The observable 
     * patient is then alerted to the response of the secretary
     */
    public void newPatient(){
        registerObserver(this);
    }
    
    /**
     * Method to remove the account of a specific patient.
     * <p>
     * The secretary receives the request from the observable patient and either
     * rejects or approves the removal (patient is identified by their unique 
     * id. Either way, the patient is informed of the decision though the 
     * observer interface again.
     * </p>
     * @param id Unique id of the patient requesting removal
     */
    public void accountRemoval(String id){

    }
    
    /**
     * Method to create an appointment between a patient and a doctor, based on
     * the preferred date and doctor supplied by the patient
     * The secretary is notified when a patient submits an appointment request
     * and then responds with a new appointment date and doctor name. Both of 
     * these routes respond to the other since both classes are linked by the
     * observer pattern.
     * @param id Unique id of the doctor to host the appointment
     * @param dateOfAppointment Preferred date for the appointment
     */
    public void createAppointment(String id, Date dateOfAppointment){

    }

    @Override
    public void updateDetails(String id, String forename, String surname, String address, String gender, Integer age, String type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Updates the stock information based on what the secretary has ordered in
     * and what the patients have collected with their prescriptions
     * @param medicine
     * @param qty 
     */
    @Override
    public void updateStock(String medicine, Integer qty) {
        this.medicine = medicine;
        this.qty = qty;
        
        //Changes the old stock value of the medicine to the new one
        notifyObserver(); //Informs any patients waiting for out of stock medicines.
    }

    /**
     * Updates the appointment information based on the patient information. 
     * The secretary reviews the appointment request and sets up a new appointment
     * (based on the information received from the patient) coforming to the clinic's
     * availability.
     * @param id actual doctor (uses the id to avoid duplicated name errors)
     * @param dateOfAppointment actual date of the appointment
     */
    @Override
    public void updateAppointment(String id, Date dateOfAppointment) {
        this.id = id;
        this.dateOfAppointment = dateOfAppointment;
        
        //Sets up a new appointment using the doctor id and date
        notifyObserver(); //Inform patient and doctor
    }

    @Override
    public void registerObserver(Observer observer) {
        //Used to register new accounts when a patient account request has been
        //approved
    }

    @Override
    public void removeObserver(Observer observer) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void notifyObserver() {
        //Updates patient welcome message with new appointment information
    }
}
