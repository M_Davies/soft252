/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FactoryModel;

import ObserverModel.Observable;
import ObserverModel.Observer;
import java.util.Date;

/**
 * Contains the user specific methods and variables for the Administrator
 * <p>
 * Crucial for separating the intended functionality between users
 * </p>
 * @author M_Davies
 */
public class Administrator extends User implements Observer, Observable{
    private String feedback = "None today!";
    private Observable observable;
        
    /**
     * Constructor to initialise the class type. It sets the type
     * of the class. I chose to implement this in the constructor
     * rather than an override method since the class isn't fully completed yet.
     */
    public Administrator() {
        setType("A");
    }

    /**
     * Accessor to retrieve the feedback string, it reverts
     * to the default instance above if a new one isn't set
     * @return feedback Feedback message to the doctor
     */
    public String getFeedback() {
        return feedback;
    }

    /**
     * Accessor to set the feedback string.
     * Note, it does not have to be included since a message is not compulsory
     * @param feedback New feedback message to the doctor
     */
    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
    
    /**
     * Creates a doctor or secretary account based on the type. Used by the
     * administrator to quickly add new accounts. If the accounts are non-patient,
     * the observable is left null to be specified when its needed (no need to 
     * add users to subscription threads that don't involve them).
     * @param type The type of account to add 
     */
    public User addAccount(String type){
        User newUser = null;
        switch (type) {
            case "A":
                {
                    //include accept or deny for the admin here (preferable here
                    //than in the view)
                    return newUser = new Administrator();
                }
            case "S":
                {
                    return newUser = new Secretary(null);
                }
            case "D":
                {
                    return newUser = new Doctor(null);
                }
            default:
                System.out.println("Invalid user type");
                return null;
        }
    }
    
    
    /**
     * Removes a doctor or secretary account based on the type of the account
     * and the relevant id that corresponds to that specific account 
     * @param type The type of account to delete
     * @param id The specific user id to delete
     */
    public void removeAccount(String type, String id){
        removeObserver(this);
    }
    
    /**
     * Based on the comments shown to the administrator,
     * a feedback message is created and sent to the doctor's account
     * @param feedback Message created by the user to send to the doctor
     */
    public void sendFeedback(String feedback){
        //The method recieves an administrators feedback comments to the doctor, 
        //notifies the doctor view and fills out the feedback there (the doctor
        //is an observer of the administrator
    }

    @Override
    public void updateDetails(String id, String forename, String surname, 
            String address, String gender, Integer age, String type) {
        
    }

    @Override
    public void updateStock(String medicine, Integer qty) {
        throw new UnsupportedOperationException("Access Denied");
    }

    @Override
    public void updateAppointment(String id, Date dateOfAppointment) {
        throw new UnsupportedOperationException("Access Denied"); 
    }

    /**
     * The two methods below are used to remove administrator, doctor or secretary 
     * accounts. 
     * @param observer Observer to be added or removed
     */
    @Override
    public void registerObserver(Observer observer) {

    }

    @Override
    public void removeObserver(Observer observer) {
        
    }
    
    /**
     * Inform the doctor of a feedback message update so they can see it when
     * they log in
     */
    @Override
    public void notifyObserver() {

    }
}
