/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FactoryModel;

/**
 * Concrete Creator class extending the abstract UserCreator. 
 * <p>
 * This subclass changes the creating behaviour of the abstract methods in UserCreator
 * depending on what type of user is selected. This is crucial since an unauthorised
 * user with access to personal information or administrator privileges is a 
 * critical security breach.
 * </p>
 * @author M_Davies
 */
public class UserFactory{

    /**
     * Constructor to reference the UserCreator as its parent class.
     */
    public UserFactory(){    
    }

    /**
     * Creator method for the factory pattern. Builds the user baser on the type
     * of the user. A null observer is also added for now
     * @param type Type of user to be created 
     * @return user type. If an error is thrown, a null user type is assigned and
     * thrown by the compiler (check setType() in User)
     */
    public User createUser(String type) {
        User user = null;
        
        switch (type) {
            case "P":
                return new Patient(null);
            case "A":
                return new Administrator();
            case "S":
                return new Secretary(null);
            case "D":
                return new Doctor(null);
            default:
                return null;
        }
    }
}
