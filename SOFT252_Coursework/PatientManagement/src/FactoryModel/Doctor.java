/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FactoryModel;

import ObserverModel.Observable;
import ObserverModel.Observer;
import ObserverModel.Stock;
import java.util.Date;

/**
 * Contains the user specific methods and variables for the Doctor
 * <p>
 * Crucial for separating the intended functionality between users
 * </p>
 * @author M_Davies
 */
public class Doctor extends User implements Observer, Observable{
    //Variables to create appointment
    private String id = getId();
    private Date dateOfAppointment = new Date();
    
    //Unique variables
    private String medicine = "Unknown";
    private Integer qty = 0;
    
    //Variables for identifying the observer and subject
    private Observable appointment; 
    private static Integer observerIDTracker = 0;
    private Integer observerID;

    /**
     * Constructor to initialise the class subjects, ID. It sets the type
     * of the class (implemented in the constructor over a override method as 
     * the class isn't complete). It also sets the doctor an observer id and
     * as an observer to the appointment class (not using the user id since
     * incrementing that would be unwise)
     * @param appointment Subject the user wishes to observe
     */
    public Doctor(Observable appointment) {
        this.appointment = appointment;
        this.observerID = ++observerIDTracker;
        appointment.registerObserver(this);
        
        setType("D"); 
    }

    /**
     * Accessor to retrieve the medicine name
     * @return medicine Medicine to be used in prescriptions
     */
    public String getMedicine() {
        return medicine;
    }

    /**
     * Accessor to set the medicine. Used when creating new prescriptions and 
     * medicines. No input validation as appointments can be completed without
     * prescribing medicines
     * @param medicine Medicine to prescribe or create
     */
    public void setMedicine(String medicine) {
        this.medicine = medicine;
    }
    
    /**
     * Method to view the records of the current patient. This method is likely
     * to be the exact same as the patient viewHistory method. To reduce, code 
     * repetition, an interface could have been added to implement these.
     * @param id Unique id of the patient
     */
    public void viewRecords(String id){
       //Views the patient history. The doctor and patient should be the only ones
       //with access to this
    }
   
    /**
     * Method to create a new medicine and request that the medicine is brought
     * into stock. The method would take the new medicine and update the secretary
     * since the doctor is an observable
     */
    public void createMedicine(){
        updateStock(medicine, qty);
   }
   
    /**
     * Method for writing appointment notes and a prescription for the patient
     * to collect from the secretary. The prescription is created and passed to
     * the prescription class to be implemented.
     * Because the secretary is an observer of the prescription class, 
     * they should be notified when this method is fired.
     * @param medicine Medicine name to prescribe 
     * @param qty Amount that should be given to the patient
     */
    public void createPrescription(String medicine, Integer qty){
        //The method passes the prescription details to the stock class then
        //informs the secretary and the patient of the change
        notifyObserver();
   }
    
    /**
     * Override methods for the observer that are denied. The doctor is not
     * authorised to create and delete accounts
     */
    @Override
    public void updateDetails(String id, String forename, String surname, 
            String address, String gender, Integer age, String type) {
        throw new UnsupportedOperationException("Access Denied");
    }

    /**
     * My attempt to allow the doctor to create a new medicine and assign it 
     * to the stock list for the secretary to order. As seen below, it implements
     * the other observer methods, not just the stock method. This lead me to 
     * believe that medicine should have been its own class from the start.
     * @param medicine New medicine name to add to the stock list
     * @param qty Quantity that needs to be supplied
     */
    @Override
    public void updateStock(String medicine, Integer qty) {
        Stock newMedicine = new Stock() {
            @Override
            public void updateDetails(String id, String forename, String surname, 
                    String address, String gender, Integer age, String type) {
                throw new UnsupportedOperationException("Access Denied"); 
            }

            @Override
            public void updateAppointment(String id, Date dateOfAppointment) {
                throw new UnsupportedOperationException("Access Denied"); 
            }
        }; 
    }

    /**
     * Creates a new appointment as a follow up from the previous one. 
     * The doctor reviews their timetable and sets up a new appointment with
     * themselves conforming to the patient's availability. Note, the "this"
     * keyword is referring to the private variable above, not the method 
     * parameter.
     * @param id same doctor as the one taking the appointment
     * @param dateOfAppointment new appointment date
     */
    @Override
    public void updateAppointment(String id, Date dateOfAppointment) {
        id = this.id;
        dateOfAppointment = this.dateOfAppointment;
        
        //Sets up a new appointment using the secretaries doctor id and date
        notifyObserver(); //Inform patient and doctor
    }

    /**
     * Override methods for the observer that are denied. The doctor is not
     * authorised to create and delete accounts
     * @param observer Patient observer of any new doctor appointments
     */
    @Override
    public void registerObserver(Observer observer) {
        throw new UnsupportedOperationException("Access Denied"); 
    }

    @Override
    public void removeObserver(Observer observer) {
        throw new UnsupportedOperationException("Access Denied."); 
    }

    @Override
    public void notifyObserver() {
        //Updates patient on any new appointments or prescriptions
        //the doctor will have created. This will be in the form of the welcome
        //message
    }
}
