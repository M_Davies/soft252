/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FactoryModel;

import ObserverModel.Observable;
import ObserverModel.Observer;
import java.util.Date;

/**
 * Contains the user specific methods and variables for the Patient
 * <p>
 * Crucial for separating the intended functionality between users
 * </p>
 * @author M_Davies
 */
public class Patient extends User implements Observer{
    //Variables to create appointment
    private String id = getId();
    private Date dateOfAppointment = new Date();
    
    //Unique variables
    private Integer age = 0;
    private String gender = "None";
    private Double rating = 0.00;
    private String message = "Nothing to add!";
    
    
    //Variables for identifying the observer and subject
    private Observable appointment; 
    private static Integer observerIDTracker = 0;
    private Integer observerID;

    /**
     * Constructor to initialise the class subjects, ID. It sets the type
     * of the class (implemented in the constructor over a override method as 
     * the class isn't complete). It also sets the patient an observer id and
     * as an observer to the appointment class (not using the patient id since
     * incrementing that would be unwise)
     * @param appointment Subject the user wishes to observe
     */
    public Patient(Observable appointment) {
        this.appointment = appointment;
        this.observerID = ++observerIDTracker;
        appointment.registerObserver(this);
        
        setType("P"); 
    }

    public Patient() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Accessor to retrieve the age.
     * @return age Age of the patient
     */
    public Integer getAge() {
        return age;
    }

    /**
     * Accessor to set the age. Includes validation checking for 
     * null or minus ages
     * @param age New age of the patient
     * 
     */
    public void setAge(Integer age) {
        if (age != null && age > 0) {
            this.age = age;
        }       
    }

    /**
     * Accessor to retrieve the gender
     * @return gender Gender of the patient
     */
    public String getGender() {
        return gender;
    }

    /**
     * Accessor to set the gender. Includes validation checking for
     * null or empty genders.
     * Note, male and female were not included as hard boundaries for genders
     * for user comfort purposes (a user may not identify as a male or female)
     * @param gender New gender of the patient
     */
    public void setGender(String gender) {
        if (gender != null && !gender.isEmpty()) {
            this.gender = gender;
        }       
    }

    /**
     * Accessor to retrieve the rating
     * @return rating Rating given to a doctor
     */
    public Double getRating() {
        return rating;
    }

    /**
     * Accessor to set the rating. Includes validation checking for null
     * or invalid ratings. A rating is considered invalid if it is outside 
     * the system standards for a doctor rating
     * @param rating New rating for the doctor
     */
    public void setRating(Double rating) {
        if (rating != null && rating > 0 && rating < 5.01) {
            this.rating = rating;
        }
    }

    /**
     * Accessor to retrieve the feedback message 
     * @return message Comments on the doctor from the patient
     */
    public String getMessage() {
        return message;
    }

    /**
     * Accessor to set the feedback message. Includes validation checking for
     * null or empty messages. A small comment must always be given here when
     * rating a doctor
     * @param message New comments on the doctor from the patient 
     */
    public void setMessage(String message) {
        if (message != null && !message.isEmpty()) {
            this.message = message;
        }        
    }

    /**
     * Accessor to retrieve the date of the appointment
     * @return dateOfAppointment Appointment date with the doctor
     */
    public Date getDateOfAppointment() {
        return dateOfAppointment;
    }

    /**
     * Accessor to set the date of the appointment with a doctor. Includes
     * validation checking for null or invalid dates. An appointment must always
     * be set after the current date.
     * @param dateOfAppointment New appointment date with the doctor
     */
    public void setDateOfAppointment(Date dateOfAppointment) {
        Date currentDate = new Date();
        if (dateOfAppointment != null && dateOfAppointment.after(currentDate)) {
            this.dateOfAppointment = dateOfAppointment;
        } 
    }
    
    /**
     * Shows the history of the patient's appointments (including active
     * appointments) to keep a compact design.
     */
    public void viewHistory(){

    }
    
    /**
     * Requests the removal of the patient account and notifies the secretary
     * of this.
     * @param id Unique id of the patient sending the request to remove
     */
    public void requestRemoval(String id){

    }
    
    /**
     * Sends a feedback message on a specific doctor to the administrator
     * @param forename Forename of the doctor
     * @param surname Surname of the doctor
     * @param rating Rating given to the doctor
     * @param message Comments on the doctor's performance to support the rating
     */
    public void sendFeedback(String forename, String surname, Double rating, 
            String message){

    }

    @Override
    public void updateDetails(String id, String forename, String surname, 
            String address, String gender, Integer age, String type) {
        throw new UnsupportedOperationException("Access Denied"); 
    }

    @Override
    public void updateStock(String medicine, Integer qty) {
        throw new UnsupportedOperationException("Access Denied");
    }

    /**
     * Uses the abstract observer class to the set a new appointment request
     * @param id requested doctor (uses the id to avoid duplicated name errors)
     * @param dateOfAppointment requested date of the appointment
     */
    @Override
    public void updateAppointment(String id, Date dateOfAppointment) {
        this.id = id;
        this.dateOfAppointment = dateOfAppointment;
        //Updates welcome message on secretary with appointment request information
    }
}
